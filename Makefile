include $(FSLCONFDIR)/default.mk

PROJNAME = susan
RUNTCLS  = Susan
XFILES   = susan
LIBS     = -lfsl-newimage -lfsl-miscmaths -lfsl-utils \
           -lfsl-NewNifti -lfsl-cprob -lfsl-znz

all: susan

susan: susan.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
